package br.com.fiap.tds;

public class Registro {
	
	/*@author Higor Alcantara Costa	
	 * dados que ser�o usados para cadastro do cliente
	 * @verson 1.0
	 */
	private String Nome;
	/*
	 * Nome da conta
	 */
	private double CPF;
	/*
	 * CPF da conta
	 */
	private double Peso ;
	/*
	 * Peso do usuario 
	 */
	private double Altura;
	/*
	 * Altura do usuario
	 */
	private String Sexo;
	/*
	 * Sexo do usuario
	 */
	private String Email;
	/*
	 * Email do usuario
	 */
	private String Senha;
	/*
	 * Senha do Usuario
	 */

	
	
	public String getNome() {
		return Nome;
	}
	public void setNome(String nome) {
		Nome = nome;
	}
	public double getCPF() {
		return CPF;
	}
	public void setCPF(double cPF) {
		CPF = cPF;
	}
	public double getPeso() {
		return Peso;
	}
	public void setPeso(double peso) {
		Peso = peso;
	}
	public double getAltura() {
		return Altura;
	}
	public void setAltura(double altura) {
		Altura = altura;
	}
	public String getSexo() {
		return Sexo;
	}
	public void setSexo(String sexo) {
		Sexo = sexo;
	}
	public String getEmail() {
		return Email;
	}
	public void setEmail(String email) {
		Email = email;
	}
	public String getSenha() {
		return Senha;
	}
	public void setSenha(String senha) {
		Senha = senha;
	}

}
	