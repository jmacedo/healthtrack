package br.com.fiap.tds;

public class Alimentacao extends Registro{
	
	//descri��o do registro de alimento e seus atributos
	private String nomeDoAlimento;
	private String descricao;
	private int data;
	private double calorias;

	
	
	public String getNomeDoAlimento() {
		return nomeDoAlimento;
	}
	public void setNomeDoAlimento(String nomeDoAlimento) {
		this.nomeDoAlimento = nomeDoAlimento;
	}
	public String getDescricao() {
		return descricao;
	}
	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}
	public int getData() {
		return data;
	}
	public void setData(int data) {
		this.data = data;
	}
	public double getCalorias() {
		return calorias;
	}
	public void setCalorias(double calorias) {
		this.calorias = calorias;
	}
	
	
}
